<?php

/**
 * @file
 * Provides themes definitions and preprocess templates for widgets on demand.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function widget_on_demand_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.widget_on_demand':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Widget On Demand module provides a widgets on demand and a framework that other modules can use to create widgets on demands.');
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Configuring field widgets') . '</dt>';
      $output .= '<dd>' . t('On the <em>Manage form display</em> page of your entity type or sub-type, you can select the widget to use for editing. Widgets on demand have additional settings next to the main widget settings.') . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Provides theme registration for themes across .inc files.
 */
function widget_on_demand_theme() {
  return [
    'widget_on_demand_form_element' => [
      'render element' => 'elements',
    ],
    'widget_on_demand_field' => [
      'render element' => 'element',
    ],
  ];
}

/**
 * Prepares variables for the widget on demand form element template.
 *
 * Default template: widget-on-demand-form-element.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: A render element representing the wrapped elements for the on
 *       on demand widget form element.
 *   - element_id: A string representing the unique id of the wrapped element.
 */
function template_preprocess_widget_on_demand_form_element(&$variables, $hook) {
  $elements = $variables['elements'];

  $variables['element_id'] = $elements['#element_id'];

  foreach (Element::children($elements) as $key) {
    $variables['content'][] = $elements[$key];
  }
}

/**
 * Prepares variables for field templates inside widget on demand form elements.
 *
 * Default template: widget-on-demand-field.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: A render element representing the field.
 */
function template_preprocess_widget_on_demand_field(&$variables, $hook) {
  $element = $variables['element'];

  // Creating variables for the template.
  $variables['field_name'] = $element['#field_name'];
  $variables['field_type'] = $element['#field_type'];
  // Always set the field label - allow themes to decide whether to display it.
  // In addition the label should be rendered but hidden to support screen
  // readers.
  $variables['label'] = $element['#title'];

  $variables['multiple'] = $element['#is_multiple'];
  $variables['is_empty'] = $element['#is_empty'];

  $variables['content'] = $element['#content'];
}
